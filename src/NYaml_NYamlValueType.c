#include "../include/NYaml.h"

// --------------------------
// enum NYaml::NYamlValueType
// --------------------------

/**
 * Find value type from value
 *
 * @param value
 * 		The value
 *
 * @return the value type
 */
NYamlValueType NYaml_NYamlValueType_FindType( const char *value )
{
	// Is it an integer
	if( NLib_Chaine_EstUnNombreEntier( value,
		10 ) )
		return NYAML_VALUE_TYPE_INTEGER;
	else if( NLib_Chaine_EstUnNombreReel( value ) )
	// Is it a real?
		return NYAML_VALUE_TYPE_DOUBLE;
	// This is a string
	else
	{
		// Is it a boolean?
		if( NLib_Chaine_Comparer( value,
			NFALSE_KEYWORD,
			NFALSE,
			0 )
			|| NLib_Chaine_Comparer( value,
				NTRUE_KEYWORD,
				NFALSE,
				0 ) )
			// This is a boolean
			return NYAML_VALUE_TYPE_BOOLEAN;
		else
			// This is a string
			return NYAML_VALUE_TYPE_STRING;
	}
}

