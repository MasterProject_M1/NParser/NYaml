#include "../../include/NYaml.h"

// -----------------------
// namespace NYaml::Output
// -----------------------

/**
 * Display nodes
 *
 * @param node
 * 		The root node
 */
void NYaml_Output_DisplayNodeArborescence( const NYamlNode *node )
{
	// Iterator
	NU32 i;

	// Node type display
	switch( node->m_nodeType )
	{
		case NYAML_NODE_TYPE_ROOT:
			printf( "[ROOT  ]" );
			break;
		case NYAML_NODE_TYPE_ARRAY:
			printf( "[ARRAY ]" );
			break;
		case NYAML_NODE_TYPE_SIMPLE_VALUE:
			printf( "[SIMPLE]" );
			break;
		case NYAML_NODE_TYPE_FULL_VALUE:
			printf( "[FULL  ]" );
			break;

		default:
			break;
	}

	// Display node depth
	printf( "[%d] ",
		node->m_depth );

	// Display node details
	switch( node->m_nodeType )
	{
		// Unknown type
		default:
			break;

			// Root/Array type
		case NYAML_NODE_TYPE_ROOT:
		case NYAML_NODE_TYPE_ARRAY:
			// Display array key
			printf( "Array key = [%s]\n",
				node->m_key != NULL ?
				node->m_key
									: "NULL" );

			// Display array nodes
			for( i = 0; i < ( (NYamlArray*)node->m_value )->m_array->m_nombre; i++ )
				NYaml_Output_DisplayNodeArborescence( (NYamlNode *)( (NYamlArray *)node->m_value )->m_array->m_element[ i ] );
			break;

			// Simple/Full value
		case NYAML_NODE_TYPE_FULL_VALUE:
		case NYAML_NODE_TYPE_SIMPLE_VALUE:
			printf( "Key = [%s], Value = [%s]\n",
				node->m_key != NULL ?
				node->m_key
									: "NULL",
				node->m_value != NULL ?
				(char*)node->m_value
									  : "NULL" );
			break;
	}
}

/**
 * Internal data chain processing
 *
 * @param node
 * 		The current node
 * @param output
 * 		The output
 * @param currentChain
 * 		The current chain
 *
 * @return if operation succedeed
 */
__PRIVATE NBOOL NYaml_CreateNodeChainInternal( const NYamlNode *node,
	__OUTPUT NParserOutputList *output,
	const char *currentChain )
{
	// New chain
	char *newChain;

	// New size
	NU32 newSize;

	// Value copy
	char *valueCopy;

	// Value type
	NParserOutputType valueType;

	// Iterator
	NU32 i;

	// Calculate new size
	newSize = (NU32)strlen( currentChain )
			  // .
			  + 1
			  // key
			  + ( ( NYaml_NYamlNode_GetKey( node ) != NULL ) ?
				  (NU32)strlen( NYaml_NYamlNode_GetKey( node ) )
															 : 0 )
			  // .
			  + 1
			  // value
			  + ( ( NYaml_NYamlNode_GetNodeType( node ) != NYAML_NODE_TYPE_ARRAY
					&& NYaml_NYamlNode_GetNodeType( node ) != NYAML_NODE_TYPE_ROOT ) ?
				  (NU32)strlen( NYaml_NYamlNode_GetValue( node ) )
																					 : 0 );

	// Build new chain
	if( !( newChain = calloc( newSize + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Copy old chain
	memcpy( newChain,
		currentChain,
		strlen( currentChain ) );

	// If key not NULL
	if( NYaml_NYamlNode_GetKey( node ) != NULL )
	{
		// Check depth
		if( NYaml_NYamlNode_GetDepth( node ) >= 1 )
			// Add .
			strcat( newChain,
				"." );

		// Add key to chain
		strcat( newChain,
			NYaml_NYamlNode_GetKey( node ) );
	}

	switch( NYaml_NYamlNode_GetNodeType( node ) )
	{
		// Unknown type
		default:
			NFREE( newChain );
			break;

			// Root/Array type
		case NYAML_NODE_TYPE_ROOT:
		case NYAML_NODE_TYPE_ARRAY:
			// Add array nodes
			for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( NYaml_NYamlArray_GetElementList( (NYamlArray*)NYaml_NYamlNode_GetValue( node ) ) ); i++ )
				NYaml_CreateNodeChainInternal( NLib_Memoire_NListe_ObtenirElementDepuisIndex( NYaml_NYamlArray_GetElementList( (NYamlArray*)NYaml_NYamlNode_GetValue( node ) ),
					i ),
					output,
					NYaml_NYamlNode_GetDepth( NLib_Memoire_NListe_ObtenirElementDepuisIndex( NYaml_NYamlArray_GetElementList( (NYamlArray*)NYaml_NYamlNode_GetValue( node ) ),
						i ) ) > NYaml_NYamlNode_GetDepth( node ) ?
						newChain
						: "" );

			// Free chain
			NFREE( newChain );
			break;

			// Simple/Full value
		case NYAML_NODE_TYPE_FULL_VALUE:
		case NYAML_NODE_TYPE_SIMPLE_VALUE:
			// Duplicate value
			switch( NYaml_NYamlNode_GetValueType( node ) )
			{
				default:
				case NYAML_VALUE_TYPE_BOOLEAN:
					// Allocate
					if( !( valueCopy = calloc( 1,
						sizeof( NBOOL ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Liberer
						NFREE( newChain );

						// Quitter
						return NFALSE;
					}

					// Parse
					valueType = NPARSER_OUTPUT_TYPE_BOOLEAN;
					( *(NBOOL*)valueCopy ) = NLib_Chaine_Comparer( NYaml_NYamlNode_GetValue( node ),
						NTRUE_KEYWORD,
						NFALSE,
						0 );
					break;

				case NYAML_VALUE_TYPE_INTEGER:
					// Allocate
					if( !( valueCopy = calloc( 1,
						sizeof( NS32 ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Liberer
						NFREE( newChain );

						// Quitter
						return NFALSE;
					}

					// Copy
					valueType = NPARSER_OUTPUT_TYPE_INTEGER;
					( *(NS32*)valueCopy ) = (NS32)strtol( NYaml_NYamlNode_GetValue( node ),
						NULL,
						10 );
					break;

				case NYAML_VALUE_TYPE_DOUBLE:
					// Allocate
					if( !( valueCopy = calloc( 1,
						sizeof( double ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Liberer
						NFREE( newChain );

						// Quitter
						return NFALSE;
					}

					// Copy
					valueType = NPARSER_OUTPUT_TYPE_DOUBLE;
					( *(double*)valueCopy ) = strtod( NYaml_NYamlNode_GetValue( node ),
						NULL );
					break;

				case NYAML_VALUE_TYPE_STRING:
					// Allocate
					if( !( valueCopy = calloc( strlen( NYaml_NYamlNode_GetValue( node ) ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Liberer
						NFREE( newChain );

						// Quitter
						return NFALSE;
					}

					// Copy
					valueType = NPARSER_OUTPUT_TYPE_STRING;
					memcpy( valueCopy,
						NYaml_NYamlNode_GetValue( node ),
						strlen( NYaml_NYamlNode_GetValue( node ) ) );
					break;
			}

			// Add new element
			NParser_Output_NParserOutputList_AddEntry( output,
				newChain,
				valueCopy,
				valueType );

			// Free
			NFREE( newChain );
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Create data chain
 *
 * @param node
 * 		The node
 *
 * @return the data chain list
 */
__ALLOC NParserOutputList *NYaml_Output_CreateNodeChain( const NYamlNode *node )
{
	// Output
	__OUTPUT NParserOutputList *out;

	// Allocate memory
	if( !( out = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Create
	if( !NYaml_CreateNodeChainInternal( node,
		out,
		"" ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Destroy list
		NParser_Output_NParserOutputList_Destroy( &out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Parse yaml string
 *
 * @param yaml
 * 		The yaml string
 *
 * @return the parser output
 */
__ALLOC NParserOutputList *NYaml_Output_Parse( const char *yaml )
{
	// Root node
	NYamlNode *node;

	// Output
	__OUTPUT struct NParserOutputList *out;

	// Parse file
	if( !( node = NYaml_Parser_Parse( yaml ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

#ifdef TEST_YAML
	NYaml_Output_DisplayNodeArborescence( node );
#endif // TEST_YAML

	// Create output
	if( !( out = NYaml_Output_CreateNodeChain( node ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NYaml_NYamlNode_Destroy( &node );

		// Quit
		return NULL;
	}

	// Free
	NYaml_NYamlNode_Destroy( &node );

	// OK
	return out;
}

/**
 * Parse yaml file
 *
 * @param filepath
 * 		The yaml file path
 *
 * @return the parser output
 */
__ALLOC NParserOutputList *NYaml_Output_Parse2( const char *filepath )
{
	// File
	NFichierBinaire *file;

	// Output
	__OUTPUT NParserOutputList *out;

	// File content
	char *fileContent;

	// Open file
	if( !( file = NLib_Fichier_NFichierBinaire_ConstruireLecture( filepath ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NULL;
	}

	// Read file content
	if( !( fileContent = NLib_Fichier_NFichierBinaire_LireTout( file ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

		// Close file
		NLib_Fichier_NFichierBinaire_Detruire( &file );

		// Quitter
		return NULL;
	}

	// Close file
	NLib_Fichier_NFichierBinaire_Detruire( &file );

	// Parse
	if( !( out = NYaml_Output_Parse( fileContent ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( fileContent );

		// Quiter
		return NULL;
	}

	// Free
	NFREE( fileContent );

	// OK
	return out;
}

