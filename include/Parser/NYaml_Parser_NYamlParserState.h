#ifndef NYAML_PARSER_NYAMLPARSERSTATE_PROTECT
#define NYAML_PARSER_NYAMLPARSERSTATE_PROTECT

// --------------------------------------
// struct NYaml::Parser::NYamlParserState
// --------------------------------------

typedef struct NYamlParserState
{
	// Node associated
	const NYamlNode *m_node;

	// Depth
	NU32 m_depth;
} NYamlParserState;

/**
 * Build parser state
 *
 * @param node
 * 		The node
 * @param depth
 * 		The depth
 *
 * @return the parser state
 */
__ALLOC NYamlParserState *NYaml_Parser_NYamlParserState_Build( const NYamlNode *node,
	NU32 depth );

/**
 * Destroy parser state
 *
 * @param this
 * 		This instance
 */
void NYaml_Parser_NYamlParserState_Destroy( NYamlParserState** );

/**
 * Get node
 *
 * @param this
 * 		This instance
 *
 * @return the node
 */
const NYamlNode *NYaml_Parser_NYamlParserState_GetNode( const NYamlParserState* );

/**
 * Get depth
 *
 * @param this
 * 		This instance
 *
 * @return the depth
 */
NU32 NYaml_Parser_NYamlParserState_GetDepth( const NYamlParserState* );

#endif // !NYAML_PARSER_NYAMLPARSERSTATE_PROTECT
