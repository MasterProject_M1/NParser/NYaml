#ifndef NYAML_NYAMLARRAY_PROTECT
#define NYAML_NYAMLARRAY_PROTECT

// ------------------------
// struct NYaml::NYamlArray
// ------------------------

typedef struct NYamlArray
{
	// Node list (NYamlNode)
	NListe *m_array;
} NYamlArray;

/**
 * Build array
 *
 * @return an empty array
 */
__ALLOC NYamlArray *NYaml_NYamlArray_Build( void );

/**
 * Destroy array
 *
 * @param this
 * 		This instance
 */
void NYaml_NYamlArray_Destroy( NYamlArray** );

/**
 * Add node from key and value
 *
 * @param this
 * 		This instance
 * @param type
 * 		The type
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param depth
 * 		The node depth
 *
 * @return if operation succedeed
 */
NBOOL NYaml_NYamlArray_AddNode( NYamlArray*,
	NYamlNodeType type,
	const char *key,
	__WILLBEOWNED char *value,
	NU32 depth );

/**
 * Add node from node
 *
 * @param this
 * 		This instance
 * @param node
 * 		The node to add
 *
 * @return if operation succedeed
 */
NBOOL NYaml_NYamlArray_AddNode2( NYamlArray*,
	__WILLBEOWNED NYamlNode *node );

/**
 * Get element list
 *
 * @param this
 * 		This instance
 *
 * @return the element list
 */
const NListe *NYaml_NYamlArray_GetElementList( const NYamlArray* );

#endif // !NYAML_NYAMLARRAY_PROTECT

