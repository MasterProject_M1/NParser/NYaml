#include "../include/NYaml.h"

// -----------------------
// struct NYaml::NYamlNode
// -----------------------

/**
 * Build node
 *
 * @param type
 * 		The node type
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param depth
 * 		The node depth
 *
 * @return the node instance
 */
__ALLOC NYamlNode *NYaml_NYamlNode_Build( NYamlNodeType type,
	const char *key,
	__WILLBEOWNED void *value,
	NU32 depth )
{
	// Output
	NYamlNode *out;

	// Check parameter
	if( value == NULL )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Allocate
	if( !( out = calloc( 1,
		sizeof( NYamlNode ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free value
		switch( type )
		{
			case NYAML_NODE_TYPE_ROOT:
			case NYAML_NODE_TYPE_ARRAY:
				NYaml_NYamlArray_Destroy( (NYamlArray**)&value );
				break;

			default:
				NFREE( value );
				break;
		}
		// Quit
		return NULL;
	}

	// Duplicate key
	switch( type )
	{
		case NYAML_NODE_TYPE_ARRAY:
		case NYAML_NODE_TYPE_FULL_VALUE:
			// Key not NULL?
			if( key != NULL )
				// Duplicate key
				if( !( out->m_key = NLib_Chaine_Dupliquer( key ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Free
					switch( type )
					{
						case NYAML_NODE_TYPE_ARRAY:
							NYaml_NYamlArray_Destroy( (NYamlArray**)&value );
							break;

						default:
							NFREE( value );
							break;
					}
					NFREE( out );

					// Quit
					return NULL;
				}
			break;

		default:
			break;
	}

	// Save
	out->m_nodeType = type;
	out->m_value = value;
	out->m_depth = depth;

	// Parse value type
	switch( out->m_nodeType )
	{
		case NYAML_NODE_TYPE_FULL_VALUE:
		case NYAML_NODE_TYPE_SIMPLE_VALUE:
			// Find type
			out->m_valueType = NYaml_NYamlValueType_FindType( out->m_value );
			break;

		default:
			break;
	}

	// OK
	return out;
}

/**
 * Destroy node
 *
 * @param this
 * 		This instance
 */
void NYaml_NYamlNode_Destroy( NYamlNode **this )
{
	// Free key/value
	switch( (*this)->m_nodeType )
	{
		case NYAML_NODE_TYPE_ROOT:
		case NYAML_NODE_TYPE_ARRAY:
			NYaml_NYamlArray_Destroy( (NYamlArray**)&(*this)->m_value );

		default:
			NFREE( (*this)->m_value );
			NFREE( (*this)->m_key );
			break;
	}

	// Free
	NFREE( (*this) );
}

/**
 * Get node type
 *
 * @param this
 * 		This instance
 *
 * @return the node type
 */
NYamlNodeType NYaml_NYamlNode_GetNodeType( const NYamlNode *this )
{
	return this->m_nodeType;
}

/**
 * Get node value type
 *
 * @param this
 * 		This instance
 *
 * @return the value type
 */
NYamlValueType NYaml_NYamlNode_GetValueType( const NYamlNode *this )
{
	return this->m_valueType;
}

/**
 * Get key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NYaml_NYamlNode_GetKey( const NYamlNode *this )
{
	return this->m_key;
}

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 * @return the value
 */
const void *NYaml_NYamlNode_GetValue( const NYamlNode *this )
{
	return this->m_value;
}

/**
 * Get depth
 *
 * @param this
 * 		This instance
 *
 * @return the depth
 */
NU32 NYaml_NYamlNode_GetDepth( const NYamlNode *this )
{
	return this->m_depth;
}