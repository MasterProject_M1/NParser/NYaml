NYaml parser
============

Goal
----

The goal is to produce a parser, being able to understand an yaml
file, and then to create an array of string (key/value) of this kind:

|Key               |Value    |
|------------------|---------|
|hosts             |HostGroup|
|vars.var1.type.etc|service1 |

Dependencies
------------

This project depends on

- NLib
- NParser
    
Data model
----------

`The node types`

    typedef enum NYamlNodeType
    {
        // Root of YAML, NULL key, NYamlArray of values
        NYAML_NODE_TYPE_ROOT,

        // Value without key (key is NULL, value is char*)
        NYAML_NODE_TYPE_SIMPLE_VALUE,
	
        // Full value (key and value are char*)
        NYAML_NODE_TYPE_FULL_VALUE,
	
        // Array node (key is char*, value is NYamlArray*)
        NYAML_NODE_TYPE_ARRAY
    } NYamlNodeType;

`A node`

    typedef struct NYamlNode
    {
        // Yaml node type
        NYamlNodeType m_nodeType;
    
        // Yaml value type (only used if node type is SIMPLE_VALUE or FULL_VALUE)
        NYamlValueType m_valueType;
    	
        // Key
        char *m_key;
    
        // Value (char* or NYamlArray* according to m_nodeType)
        void *m_value;
    
        // Depth
        NU32 m_depth;
    } NYamlNode;

`An array of values`

    typedef struct NYamlArray
    {
        // The array of NYamlNode*
        NListe *m_array;
    } NYamlArray;

Parsing data model example
--------------------------

For this file:

    ---
    - hosts: First
      name: Playbook name
      vars:
        var1: value 1
        var2:
          - v1
          - v2
        var3:
          m1: salut
          m2: ca
          m3: va?

      roles:
        - ghost.test
    ...

It will generate the following data structure:

    NYamlNode(
        NYAML_NODE_TYPE_ROOT:NULL:NYamlArray[
            NYamlNode(
                NYAML_NODE_TYPE_SIMPLE_VALUE:hosts:First
            ),
            NYamlNode(
                NYAML_NODE_TYPE_SIMPLE_VALUE:name:Playbook
            ),
            NYamlNode(
                NYAML_NODE_TYPE_ARRAY:vars:NYamlArray[
                    NYamlNode(
                        NYAML_NODE_TYPE_SIMPLE_VALUE:var1:value 1
                    ),
                    NYamlNode(
                        NYAML_NODE_TYPE_ARRAY:var2:NYamlArray[
                            NYamlNode(
                                NYAML_NODE_TYPE_FULL_VALUE:NULL:v1
                            ),
                            NYamlNode(
                                NYAML_NODE_TYPE_FULL_VALUE:NULL:v2
                            )
                        ]
                    ),
                    NYamlNode(
                        NYAML_NODE_TYPE_ARRAY:var3:NYamlArray[
                            NYamlNode(
                                NYAML_NODE_TYPE_SIMPLE_VALUE:m1:salut
                            ),
                            NYamlNode(
                                NYAML_NODE_TYPE_SIMPLE_VALUE:m2:ca
                            ),
                            NYamlNode(
                                NYAML_NODE_TYPE_SIMPLE_VALUE:m3:va?
                            )
                        ]
                    )
                ]
            ),
            NYamlNode(
                NYAML_NODE_TYPE_ARRAY:roles:NYamlArray[
                    NYamlNode(
                        NYAML_NODE_TYPE_FULL_VALUE:NULL:ghost.test
                    )
                ]
            )
        ]
    )
    
And then the following key/value array:

|Key             |Value        |
|----------------|-------------|
|hosts           |First        |
|name            |Playbook name|
|vars.var1       |value 1      |
|vars.var2.v1    |NULL         |
|vars.var2.v2    |NULL         |
|vars.var3.m1    |salut        |
|vars.var3.m2    |ca           |
|vars.var3.m3    |va           |
|roles.ghost.test|NULL         |

Parser line analysis
--------------------

The parser takes on each step one line, and processes it.

In order to create a valid node when taking one line outside
of any previous context, this logical table was made,
being able to tell which type of line we're in front of, and
then, to process it to create the right node type with
extracted key and value.

![Schema]( .image/LineAnalysis.png )

Parsing method and parser state
-------------------------------

To process data in the correct way, the parser has to save
which data is at which depth all along the parsing. The only
data saved in the state array are the root data and the array
data.

These nodes are saved with their depth, and the parser read
state array from bottom to top. When a new node is added, it
looks for the current depth - 1 last array in state array, and
then add new node to it (at the end). If the new node is an
array, it is added to the parser state array to be used by
deeper node coming later.

There is a special case for the ARRAY_START_WITH_VALUE line type
which has to, first, create an empty array and add it to the
first currentDepth - 1 array from bottom parser state list,
and then, add a full value (key + value) to it. This is the only
special case to be seen.

Author
------

Soares Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NParser/NYaml/
