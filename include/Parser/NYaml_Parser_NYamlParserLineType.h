#ifndef NYAML_PARSER_NYAMLPARSERLINETYPE_PROTECT
#define NYAML_PARSER_NYAMLPARSERLINETYPE_PROTECT

// ---------------------------------------
// enum NYaml::Parser::NYamlParserLineType
// ---------------------------------------

typedef enum NYamlParserLineType
{
	// A simple value (Key:Value)
	NYAML_PARSER_LINE_TYPE_FULL_VALUE,

	// An array (Key:)
	NYAML_PARSER_LINE_TYPE_ARRAY_START_WITHOUT_VALUE,

	// An array with a first value (- Key1:FirstValue)
	NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE,

	// An array element (- element)
	NYAML_PARSER_LINE_TYPE_ARRAY_VALUE,

	NYAML_PARSER_LINE_TYPES
} NYamlParserLineType;

#endif // !NYAML_PARSER_NYAMLPARSERLINETYPE_PROTECT

