#include "../../include/NYaml.h"

// ------------------------------------------
// struct NYaml::Parser::NYamlParserStateList
// ------------------------------------------

/**
 * Build parser state list
 *
 * @return an empty parser state list
 */
__ALLOC NYamlParserStateList *NYaml_Parser_NYamlParserStateList_Build( void )
{
	// Output
	__OUTPUT NYamlParserStateList *out;

	// Allocate
	if( !( out = calloc( 1,
		sizeof( NYamlParserStateList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_state = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NYaml_Parser_NYamlParserState_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy parser
 *
 * @param this
 * 		This instance
 */
void NYaml_Parser_NYamlParserStateList_Destroy( NYamlParserStateList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_state );

	// Free
	NFREE( *this );
}

/**
 * Add an element to list
 *
 * @param node
 * 		The node to add
 * @param depth
 * 		The depth of the node
 *
 * @return if the add succedeed
 */
NBOOL NYaml_Parser_NYamlParserStateList_AddElement( NYamlParserStateList *this,
	const NYamlNode *node,
	NU32 depth )
{
	// The parser state
	NYamlParserState *state;

	// Create state
	if( !( state = NYaml_Parser_NYamlParserState_Build( node,
		depth ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add state
	if( !NLib_Memoire_NListe_Ajouter( this->m_state,
		state ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Look for index for state of given depth (private)
 *
 * @param this
 * 		This instance
 * @param depth
 * 		The depth to look for
 *
 * @return the index of the state OR NERREUR if non existent
 */
__PRIVATE NU32 NYaml_Parser_NYamlParserStateList_LookForIndexFromDepth( const NYamlParserStateList *this,
	NU32 depth )
{
	// Output
	NU32 out;

	// Look for, starting from the end
	for( out = NLib_Memoire_NListe_ObtenirNombre( this->m_state ) - 1; (NS32)out >= 0; out-- )
		// Check
		if( NYaml_Parser_NYamlParserState_GetDepth( (NYamlParserState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_state,
				out ) ) == depth )
			// OK
			return out;

	// Error
	return NERREUR;
}

/**
 * Get state for given depth
 *
 * @param this
 * 		This instance
 * @param depth
 * 		The state depth looked for
 *
 * @return the state or NULL if non existent
 */
const NYamlParserState *NYaml_Parser_NYamlParserStateList_GetState( const NYamlParserStateList *this,
	NU32 depth )
{
	// State index
	NU32 index;

	// Look for element with this depth
	if( ( index = NYaml_Parser_NYamlParserStateList_LookForIndexFromDepth( this,
		depth ) ) == NERREUR )
		return NULL;

	// OK
	return (NYamlParserState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_state,
		index );
}

