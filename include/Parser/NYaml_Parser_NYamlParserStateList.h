#ifndef NYAML_PARSER_NYAMLPARSERSTATELIST_PROTECT
#define NYAML_PARSER_NYAMLPARSERSTATELIST_PROTECT

// ------------------------------------------
// struct NYaml::Parser::NYamlParserStateList
// ------------------------------------------

typedef struct NYamlParserStateList
{
	// State list
	NListe *m_state;
} NYamlParserStateList;

/**
 * Build parser state list
 *
 * @return an empty parser state list
 */
__ALLOC NYamlParserStateList *NYaml_Parser_NYamlParserStateList_Build( void );

/**
 * Destroy parser
 *
 * @param this
 * 		This instance
 */
void NYaml_Parser_NYamlParserStateList_Destroy( NYamlParserStateList** );

/**
 * Add an element to list
 *
 * @param node
 * 		The node to add
 * @param depth
 * 		The depth of the node
 *
 * @return if the add succedeed
 */
NBOOL NYaml_Parser_NYamlParserStateList_AddElement( NYamlParserStateList*,
	const NYamlNode *node,
	NU32 depth );

/**
 * Get state for given depth
 *
 * @param this
 * 		This instance
 * @param depth
 * 		The state depth looked for
 *
 * @return the state or NULL if non existent
 */
const NYamlParserState *NYaml_Parser_NYamlParserStateList_GetState( const NYamlParserStateList*,
	NU32 depth );

#endif // !NYAML_PARSER_NYAMLPARSERSTATELIST_PROTECT
