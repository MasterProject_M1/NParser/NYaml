#ifndef NYAML_OUTPUT_PROTECT
#define NYAML_OUTPUT_PROTECT

// -----------------------
// namespace NYaml::Output
// -----------------------

/**
 * Display nodes
 *
 * @param node
 * 		The root node
 */
void NYaml_Output_DisplayNodeArborescence( const NYamlNode *node );

/**
 * Create data chain
 *
 * @param node
 * 		The node
 *
 * @return the data chain list
 */
__ALLOC NParserOutputList *NYaml_Output_CreateNodeChain( const NYamlNode *node );

/**
 * Parse yaml string
 *
 * @param yaml
 * 		The yaml string
 *
 * @return the parser output
 */
__ALLOC NParserOutputList *NYaml_Output_Parse( const char *yaml );

/**
 * Parse yaml file
 *
 * @param filepath
 * 		The yaml file path
 *
 * @return the parser output
 */
__ALLOC NParserOutputList *NYaml_Output_Parse2( const char *filepath );

#endif // !NYAML_OUTPUT_PROTECT
