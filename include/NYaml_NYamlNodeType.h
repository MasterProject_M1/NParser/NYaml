#ifndef NYAML_NYAMLNODETYPE_PROTECT
#define NYAML_NYAMLNODETYPE_PROTECT

// -------------------------
// enum NYaml::NYamlNodeType
// -------------------------

typedef enum NYamlNodeType
{
	// Root of YAML, NULL key, NYamlArray of values
	NYAML_NODE_TYPE_ROOT,

	// Value without key (key is NULL, value is char*)
	NYAML_NODE_TYPE_SIMPLE_VALUE,

	// Full value (key and value are char*)
	NYAML_NODE_TYPE_FULL_VALUE,

	// Array node (key is char*, value is NYamlArray*)
	NYAML_NODE_TYPE_ARRAY,

	NYAML_NODE_TYPES
} NYamlNodeType;

#endif // !NYAML_NYAMLNODETYPE_PROTECT
