#include "../include/NYaml.h"

// ---------------
// namespace NYaml
// ---------------

#ifdef TEST_YAML
#define TEST_FILE_COUNT		2
static const char TestFilePath[ TEST_FILE_COUNT ][ 32 ] =
{
	"test/test.yml",
	"test/test2.yml"
};

/**
 * Entry point for test
 *
 * @param argc
 * 		The argument count
 * @param argv
 * 		The argument vector
 *
 * @return EXIT_SUCCESS if OK
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// List
	NParserOutputList *list;

	// Iterators
	NU32 i,
		j;

	// Init NLib
	NLib_Initialiser( NULL );

	// Parse files
	for( j = 0; j < TEST_FILE_COUNT; j++ )
	{
		// Parse
		printf( "Parsing [%s]...\n",
			TestFilePath[ j ] );
		if( !( list = NYaml_Output_Parse2( TestFilePath[ j ] ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

			// Quit
			return EXIT_FAILURE;
		}

		// Display
		for( i = 0; i < list->m_list->m_nombre; i++ )
		{
			printf( "[%s]:[",
				( (NParserOutput **)list->m_list->m_element )[ i ]->m_key );

			switch( ( (NParserOutput **)list->m_list->m_element )[ i ]->m_type )
			{
				default:
				case NPARSER_OUTPUT_TYPE_BOOLEAN:
					printf( "BOOLEAN][%d",
						*( (NBOOL *)( (NParserOutput **)list->m_list->m_element )[ i ]->m_value ) );
					break;

				case NPARSER_OUTPUT_TYPE_INTEGER:
					printf( "INTEGER][%d",
						*( (NS32 *)( (NParserOutput **)list->m_list->m_element )[ i ]->m_value ) );
					break;

				case NPARSER_OUTPUT_TYPE_DOUBLE:
					printf( "DOUBLE][%f",
						*( (double *)( (NParserOutput **)list->m_list->m_element )[ i ]->m_value ) );
					break;

				case NPARSER_OUTPUT_TYPE_STRING:
					printf( "STRING][%s",
						( (NParserOutput **)list->m_list->m_element )[ i ]->m_value );
					break;
			}
			puts( "]" );
		}

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &list );
	}

	// Quit NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // TEST_YAML

