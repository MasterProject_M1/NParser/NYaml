#ifndef NYAML_NYAMLNODE_PROTECT
#define NYAML_NYAMLNODE_PROTECT

// -----------------------
// struct NYaml::NYamlNode
// -----------------------

typedef struct NYamlNode
{
	// Yaml node type
	NYamlNodeType m_nodeType;

	// Yaml value type (only used if node type is SIMPLE_VALUE or FULL_VALUE)
	NYamlValueType m_valueType;

	// Key
	char *m_key;

	// Value (char* or NYamlArray* according to m_nodeType)
	void *m_value;

	// Depth
	NU32 m_depth;
} NYamlNode;

/**
 * Build node
 *
 * @param type
 * 		The node type
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param depth
 * 		The node depth
 *
 * @return the node instance
 */
__ALLOC NYamlNode *NYaml_NYamlNode_Build( NYamlNodeType type,
	const char *key,
	__WILLBEOWNED void *value,
	NU32 depth );

/**
 * Destroy node
 *
 * @param this
 * 		This instance
 */
void NYaml_NYamlNode_Destroy( NYamlNode** );

/**
 * Get node type
 *
 * @param this
 * 		This instance
 *
 * @return the node type
 */
NYamlNodeType NYaml_NYamlNode_GetNodeType( const NYamlNode * );

/**
 * Get node value type
 *
 * @param this
 * 		This instance
 *
 * @return the value type
 */
NYamlValueType NYaml_NYamlNode_GetValueType( const NYamlNode* );

/**
 * Get key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NYaml_NYamlNode_GetKey( const NYamlNode* );

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 * @return the value
 */
const void *NYaml_NYamlNode_GetValue( const NYamlNode* );

/**
 * Get depth
 *
 * @param this
 * 		This instance
 *
 * @return the depth
 */
NU32 NYaml_NYamlNode_GetDepth( const NYamlNode* );

#endif // !NYAML_NYAMLNODE_PROTECT
