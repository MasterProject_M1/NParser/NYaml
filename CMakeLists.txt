# Set minimal cmake version
cmake_minimum_required( VERSION
		3.1.3 )

# Set output folder
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )

# NLib source files
file( GLOB_RECURSE
		NLIB_SOURCE_FILES
		../../NLib/NLib/NLib/*.c )
file( GLOB_RECURSE
		NLIB_HEADER_FILES
		../../NLib/NLib/NLib/*.h )

# NYaml parser source files
file( GLOB_RECURSE
		NYAML_PARSER_SOURCE_FILES
		src/*.c )
file( GLOB_RECURSE
		NYAML_PARSER_HEADER_FILES
		include/*.h )

# NParser source files
file( GLOB_RECURSE
		NPARSER_SOURCE_FILES
		../NParser/*.c )
file( GLOB_RECURSE
		NPARSER_HEADER_FILES
		../NParser/*.h )

# Remove general output source files
list( REMOVE_ITEM
	NPARSER_SOURCE_FILES
	${CMAKE_CURRENT_SOURCE_DIR}/../NParser/src/NParser.c )

# Test
project( TEST_YAML )
	# Source files list
	add_executable( TestYaml
		${NLIB_SOURCE_FILES}
		${NLIB_HEADER_FILES}
		${NYAML_PARSER_SOURCE_FILES}
		${NYAML_PARSER_HEADER_FILES}
		${NPARSER_SOURCE_FILES}
		${NPARSER_HEADER_FILES} )

	target_compile_definitions( TestYaml
		PUBLIC
		TEST_YAML )

	# Link libraries
	target_link_libraries( TestYaml
		pthread )