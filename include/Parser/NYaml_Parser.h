#ifndef NYAML_PARSER_PROTECT
#define NYAML_PARSER_PROTECT

// -----------------------
// namespace NYaml::Parser
// -----------------------

// enum NYaml::Parser::NYamlParserLineType
#include "NYaml_Parser_NYamlParserLineType.h"

// struct NYaml::Parser::NYamlParserState
#include "NYaml_Parser_NYamlParserState.h"

// struct NYaml::Parser::NYamlParserStateList
#include "NYaml_Parser_NYamlParserStateList.h"

/**
 * Parse an YAML string
 *
 * @param yaml
 * 		The yaml string
 *
 * @return a NYamlNode with ROOT type
 */
__ALLOC NYamlNode *NYaml_Parser_Parse( const char *yaml );

#endif // !NYAML_PARSER_PROTECT

