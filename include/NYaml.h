#ifndef NYAML_PROTECT
#define NYAML_PROTECT

// ---------------
// namespace NYaml
// ---------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NParser
#include "../../NParser/include/NParser.h"

// enum NYaml::NYamlNodeType
#include "NYaml_NYamlNodeType.h"

// enum NYaml::NYamlValueType
#include "NYaml_NYamlValueType.h"

// struct NYaml::NYamlNode
#include "NYaml_NYamlNode.h"

// struct NYaml::NYamlArray
#include "NYaml_NYamlArray.h"

// namespace NYaml::Parser
#include "Parser/NYaml_Parser.h"

// namespace NYaml::Output
#include "Output/NYaml_Output.h"

#endif // !NYAML_PROTECT

