#ifndef NYAML_NYAMLVALUETYPE_PROTECT
#define NYAML_NYAMLVALUETYPE_PROTECT

// --------------------------
// enum NYaml::NYamlValueType
// --------------------------

typedef enum NYamlValueType
{
	NYAML_VALUE_TYPE_STRING,
	NYAML_VALUE_TYPE_INTEGER,
	NYAML_VALUE_TYPE_DOUBLE,
	NYAML_VALUE_TYPE_BOOLEAN,

	NYAML_VALUE_TYPES
} NYamlValueType;

/**
 * Find value type from value
 *
 * @param value
 * 		The value
 *
 * @return the value type
 */
NYamlValueType NYaml_NYamlValueType_FindType( const char *value );

#endif // !NYAML_NYAMLVALUETYPE_PROTECT

