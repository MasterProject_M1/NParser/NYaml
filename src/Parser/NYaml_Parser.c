#include "../../include/NYaml.h"

// -----------------------
// namespace NYaml::Parser
// -----------------------

/**
 * Get the line type
 *
 * @param line
 * 		The read line WITHOUT INITIAL SPACES
 *
 * @return the line type
 */
__PRIVATE NYamlParserLineType NYaml_Parser_GetLineType( const char *line )
{
	// Output
	NYamlParserLineType type = NYAML_PARSER_LINE_TYPES;

	// Is first character a -?
	NBOOL isFirstCharacterMinus;

	// Is character : present?
	NBOOL isSemicolonPresent = NFALSE;

	// Is second part in string present
	NBOOL isValuePresentInString = NFALSE;

	// Cursor
	NU32 cursor = 0;

	// Look for minus character
	isFirstCharacterMinus = (NBOOL)( line[ 0 ] == '-' );

	// Look for semicolon
	if( NLib_Chaine_PlacerAuCaractere( line,
		':',
		&cursor ) )
	{
		// Semicolon present
		isSemicolonPresent = NTRUE;

		// Check if there is text after :
		isValuePresentInString = (NBOOL)( NLib_Chaine_ObtenirProchainCaractere( line,
			&cursor,
			NFALSE ) != '\0' );
	}

	// Compute analysis
		// Key:[ ]
			if( ( !isFirstCharacterMinus
				&& isSemicolonPresent
				&& !isValuePresentInString )
				|| ( isFirstCharacterMinus
				   && isSemicolonPresent
				   && !isValuePresentInString ) )
				type = NYAML_PARSER_LINE_TYPE_ARRAY_START_WITHOUT_VALUE;
		// Key:Value
			if( !isFirstCharacterMinus
				&& isSemicolonPresent
				&& isValuePresentInString )
				type = NYAML_PARSER_LINE_TYPE_FULL_VALUE;
		// - VALUE
			if( isFirstCharacterMinus
				&& !isSemicolonPresent
				&& !isValuePresentInString )
				type = NYAML_PARSER_LINE_TYPE_ARRAY_VALUE;
		// [ KEY:VALUE ]
			if( isFirstCharacterMinus
				&& isSemicolonPresent
				&& isValuePresentInString )
				type = NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE;

	// OK
	return type;
}

/**
 * Create a node from line (private)
 *
 * @param line
 * 		The read line WITHOUT BEGINNING SPACE
 * @param lineType
 * 		The line type
 * @param depth
 * 		The current depth
 *
 * @return the node
 */
__PRIVATE __ALLOC NYamlNode *NYaml_Parser_CreateNodeFromLine( const char *line,
	NYamlParserLineType lineType,
	NU32 depth )
{
	// Key
	char *key = NULL;

	// Value
	char *value = NULL;

	// Node value
	void *nodeValue = NULL;

	// Line copy
	char *lineCopy;

	// Cursor
	NU32 cursor = 0;

	// Array
	NYamlArray *array;

	// Node
	NYamlNode *node;
	NYamlNodeType nodeType;

	// Process node type
	switch( lineType )
	{
		case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE:
		case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITHOUT_VALUE:
			nodeType = NYAML_NODE_TYPE_ARRAY;
			break;
		case NYAML_PARSER_LINE_TYPE_ARRAY_VALUE:
			nodeType = NYAML_NODE_TYPE_SIMPLE_VALUE;
			break;
		case NYAML_PARSER_LINE_TYPE_FULL_VALUE:
			nodeType = NYAML_NODE_TYPE_FULL_VALUE;
			break;

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quit
			return NULL;
	}

	// Duplicate line
	if( !( lineCopy = NLib_Chaine_Dupliquer( line ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NULL;
	}

	// Read key
		// Remove '-'
			switch( lineType )
			{
				case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE:
				case NYAML_PARSER_LINE_TYPE_ARRAY_VALUE:
					lineCopy[ 0 ] = ' ';
					break;

				default:
					break;
			}
		// Set cursor before first non-separator character
			NLib_Chaine_PlacerCurseurProchainCaractereNonSeparateur( lineCopy,
				&cursor );
		// Read key
			if( !( key = NLib_Chaine_LireJusqua( lineCopy,
				':',
				&cursor,
				NFALSE ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Free
				NFREE( lineCopy );

				// Quit
				return NULL;
			}

	// Read value
	switch( lineType )
	{
		default:
			break;

		case NYAML_PARSER_LINE_TYPE_ARRAY_VALUE:
			// Recover value
			value = key;

			// Forget key
			NDISSOCIER_ADRESSE( key );
			break;

		case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE:
		case NYAML_PARSER_LINE_TYPE_FULL_VALUE:
			// Set cursor before first non-separator character
			NLib_Chaine_PlacerCurseurProchainCaractereNonSeparateur( lineCopy,
				&cursor );

			// Read value
			if( !( value = NLib_Chaine_LireJusqua( lineCopy,
				'\0',
				&cursor,
				NFALSE ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Free
				NFREE( key );
				NFREE( lineCopy );

				// Quit
				return NULL;
			}
			break;
	}

	// Create node value
	switch( lineType )
	{
		default:
			break;

		case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITHOUT_VALUE:
		case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE:
			// Build array
			if( !( array = NYaml_NYamlArray_Build( ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Free
				NFREE( value );
				NFREE( key );
				NFREE( lineCopy );

				// Quit
				return NULL;
			}

			// Add value in array if with value
			if( lineType == NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE )
			{
				// Add node to new array
 				if( !NYaml_NYamlArray_AddNode( array,
					NYAML_NODE_TYPE_FULL_VALUE,
					key,
					value,
					depth + 1 ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Free
					NYaml_NYamlArray_Destroy( &array );
					NFREE( key );
					NFREE( lineCopy );

					// Quit
					return NULL;
				}

				// Remove key
				NFREE( key );
			}

			// Save
			nodeValue = array;
			break;

		case NYAML_PARSER_LINE_TYPE_ARRAY_VALUE:
		case NYAML_PARSER_LINE_TYPE_FULL_VALUE:
			nodeValue = value;
			break;
	}

	// Create the node
	if( !( node = NYaml_NYamlNode_Build( nodeType,
		key,
		nodeValue,
		depth ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( lineCopy );
		NFREE( key );

		// Quit
		return NULL;
	}

	// Free
	NFREE( lineCopy );
	NFREE( key );

	// OK
	return node;
}

/**
 * Add node from line WITHOUT beginning spaces
 *
 * @param line
 * 		The line WITHOUT beginning spaces
 * @param lineType
 * 		The line type
 * @param depth
 * 		The depth
 * @param parserState
 * 		The parser state
 *
 * @return if the operation succedeed
 */
__PRIVATE NBOOL NYaml_Parser_AddNodeFromLine( const char *line,
	NYamlParserLineType lineType,
	NU32 depth,
	NYamlParserStateList *parserState )
{
	// The node
	NYamlNode *node;

	// The node where to add new node
	NYamlNode *nodeWhereToAdd;

	// Current state
	const NYamlParserState *currentState;

	// Create node
	if( !( node = NYaml_Parser_CreateNodeFromLine( line,
		lineType,
		depth ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Get current parser state
	if( !( currentState = NYaml_Parser_NYamlParserStateList_GetState( parserState,
		(NS32)( depth - 1 ) > 0 ?
			depth - 1
			: 0 ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Free
		NYaml_NYamlNode_Destroy( &node );

		// Quit
		return NFALSE;
	}

	// Get node where to add
	switch( NYaml_NYamlNode_GetNodeType( nodeWhereToAdd = (NYamlNode *)NYaml_Parser_NYamlParserState_GetNode( currentState ) ) )
	{
		case NYAML_NODE_TYPE_ARRAY:
		case NYAML_NODE_TYPE_ROOT:
			// Add node to current state last array
			NYaml_NYamlArray_AddNode2( (NYamlArray*)NYaml_NYamlNode_GetValue( nodeWhereToAdd ),
				node );
			break;

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Free
			NYaml_NYamlNode_Destroy( &node );

			// Quit
			return NFALSE;
	}

	// If array node
	if( NYaml_NYamlNode_GetNodeType( node ) == NYAML_NODE_TYPE_ARRAY )
		if( !NYaml_Parser_NYamlParserStateList_AddElement( parserState,
			node,
			depth ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Free
			NYaml_NYamlNode_Destroy( &node );

			// Quit
			return NFALSE;
		}

	// OK
	return NTRUE;
}

/**
 * Create root node (private)
 *
 * @return the root node
 */
__PRIVATE __ALLOC NYamlNode *NYaml_Parser_CreateRootNode( void )
{
	// Output
	__OUTPUT NYamlNode *node;

	// Array
	NYamlArray *array;

	// Create array
	if( !( array = NYaml_NYamlArray_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Create node
	if( !( node = NYaml_NYamlNode_Build( NYAML_NODE_TYPE_ROOT,
		NULL,
		array,
		0 ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// OK
	return node;
}

/**
 * Parse an YAML string
 *
 * @param yaml
 * 		The yaml string
 *
 * @return a NYamlNode with ROOT type
 */
__ALLOC NYamlNode *NYaml_Parser_Parse( const char *yaml )
{
	// Output
	__OUTPUT NYamlNode *out = NULL;

	// Curseur
	NU32 curseur = 0;

	// Parser state
	NYamlParserStateList *parserState;

	// Line count
	NU32 lineCount = 0;

	// Space count on line
	NU32 spaceCount;

	// Depth
	NU32 depth;

	// Line
	char *line;
	NYamlParserLineType lineType;

	// Create parser state
	if( !( parserState = NYaml_Parser_NYamlParserStateList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Create the root node
	if( !( out = NYaml_Parser_CreateRootNode( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

		// Quit
		return NULL;
	}

	// Add root node to parser state
	if( !NYaml_Parser_NYamlParserStateList_AddElement( parserState,
		out,
		0 ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NYaml_NYamlNode_Destroy( &out );
		NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

		// Quit
		return NULL;
	}

	// Read lines
	while( ( line = NLib_Chaine_LireProchaineLigneSansCommentaire( yaml,
		&curseur,
		NFALSE,
		'#' ) ) )
	{
		// Process line
		switch( lineCount )
		{
			case 0:
				// Check header
				if( !NLib_Chaine_Comparer( line,
					"---",
					NTRUE,
					0 ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Free
					NFREE( line );
					NYaml_NYamlNode_Destroy( &out );
					NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

					// Quit
					return NULL;
				}
				break;

			default:
				// Space count
				if( ( spaceCount = NLib_Chaine_CompterNombreCaractereDebut( line,
					' ' ) ) % 2 )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_SYNTAX );

					// Free
					NFREE( line );
					NYaml_NYamlNode_Destroy( &out );
					NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

					// Quit
					return NULL;
				}

				// Calculate depth
				depth = spaceCount / 2;

				// Process line
				switch( ( lineType = NYaml_Parser_GetLineType( line + spaceCount ) ) )
				{
					case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITH_VALUE:
					case NYAML_PARSER_LINE_TYPE_ARRAY_START_WITHOUT_VALUE:
					case NYAML_PARSER_LINE_TYPE_ARRAY_VALUE:
					case NYAML_PARSER_LINE_TYPE_FULL_VALUE:
						NYaml_Parser_AddNodeFromLine( line + spaceCount,
							lineType,
							depth,
							parserState );
						break;

					default:
						// Check if this is the footer
						if( !NLib_Chaine_Comparer( line + spaceCount,
							"...",
							NTRUE,
							0 ) )
						{
							// Notify
							NOTIFIER_ERREUR( NERREUR_SYNTAX );

							// Free
							NFREE( line );
							NYaml_NYamlNode_Destroy( &out );
							NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

							// Quit
							return NULL;
						}
						break;
				}
				break;
		}

		// Increase line count
		lineCount++;

		// Free line
		NFREE( line );
	}

	// Close file
	NYaml_Parser_NYamlParserStateList_Destroy( &parserState );

	// OK
	return out;
}

