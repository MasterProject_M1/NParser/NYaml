#include "../../include/NYaml.h"

// --------------------------------------
// struct NYaml::Parser::NYamlParserState
// --------------------------------------

/**
 * Build parser state
 *
 * @param node
 * 		The node
 * @param depth
 * 		The depth
 *
 * @return the parser state
 */
__ALLOC NYamlParserState *NYaml_Parser_NYamlParserState_Build( const NYamlNode *node,
	NU32 depth )
{
	// Output
	__ALLOC NYamlParserState *out;

	// Allocate
	if( !( out = calloc( 1,
		sizeof( NYamlParserState ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_node = node;
	out->m_depth = depth;

	// OK
	return out;
}

/**
 * Destroy parser state
 *
 * @param this
 * 		This instance
 */
void NYaml_Parser_NYamlParserState_Destroy( NYamlParserState **this )
{
	NFREE( *this );
}

/**
 * Get node
 *
 * @param this
 * 		This instance
 *
 * @return the node
 */
const NYamlNode *NYaml_Parser_NYamlParserState_GetNode( const NYamlParserState *this )
{
	return this->m_node;
}

/**
 * Get depth
 *
 * @param this
 * 		This instance
 *
 * @return the depth
 */
NU32 NYaml_Parser_NYamlParserState_GetDepth( const NYamlParserState *this )
{
	return this->m_depth;
}

