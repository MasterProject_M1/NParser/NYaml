#include "../include/NYaml.h"

// ------------------------
// struct NYaml::NYamlArray
// ------------------------

/**
 * Build array
 *
 * @return an empty array
 */
__ALLOC NYamlArray *NYaml_NYamlArray_Build( void )
{
	// Output
	NYamlArray *out;

	// Allocate
	if( !( out = calloc( 1,
		sizeof( NYamlArray ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build array
	if( !( out->m_array = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NYaml_NYamlNode_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy array
 *
 * @param this
 * 		This instance
 */
void NYaml_NYamlArray_Destroy( NYamlArray **this )
{
	// Free
	NLib_Memoire_NListe_Detruire( &(*this)->m_array );
	NFREE( (*this) );
}

/**
 * Add node from key and value
 *
 * @param this
 * 		This instance
 * @param type
 * 		The type
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param depth
 * 		The node depth
 *
 * @return if operation succedeed
 */
NBOOL NYaml_NYamlArray_AddNode( NYamlArray *this,
	NYamlNodeType type,
	const char *key,
	__WILLBEOWNED char *value,
	NU32 depth )
{
	// Node
	NYamlNode *node;

	// Create node
	if( !( node = NYaml_NYamlNode_Build( type,
		key,
		value,
		depth ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add node
	return NYaml_NYamlArray_AddNode2( this,
		node );
}

/**
 * Add node from node
 *
 * @param this
 * 		This instance
 * @param node
 * 		The node to add
 *
 * @return if operation succedeed
 */
NBOOL NYaml_NYamlArray_AddNode2( NYamlArray *this,
	__WILLBEOWNED NYamlNode *node )
{
	// Add node
	return NLib_Memoire_NListe_Ajouter( this->m_array,
		node );
}

/**
 * Get element list
 *
 * @param this
 * 		This instance
 *
 * @return the element list
 */
const NListe *NYaml_NYamlArray_GetElementList( const NYamlArray *this )
{
	return this->m_array;
}
